#!/usr/bin/env bash

function req {
  IP=$1
  FILE=$2
  REVERSED=`echo $IP | awk -F . '{print $4"."$3"."$2"."$1".score.senderscore.com"}'`
  SCORE=$(curl -fsSL "https://1.1.1.1/dns-query?ct=application/dns-json&name=$REVERSED&type=A" | jq --raw-output '.Answer[0].data' | awk -F . '{print $4}')
  if [ -n "$SCORE" ]; then
    echo "sender_score_score{ip=\"$IP\"} $SCORE" >> $FILE
  else
    echo "sender_score_score{ip=\"$IP\"} 0" >> $FILE
  fi
}

IPS=(
  "1.1.1.1"
  "1.0.0.1"
)

echo "# HELP sender_score_score IP address score on ReturnPath SenderScore service" > $1
echo "# TYPE sender_score_score gauge" >> $1

for i in ${IPS[@]}; do
  req ${i} $1
done

echo "# HELP sender_score_ok It's all fine?" >> $1
echo "# TYPE sender_score_ok gauge" >> $1
echo "sender_score_ok 1" >> $1
