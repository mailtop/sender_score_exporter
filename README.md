# Sender Score Exporter for GitLab CI

Checks the score of a list of IPs in the ReturnPath Sender Score service, and
makes the result available in the Prometheus format.

## How to use

The configuration must be done in three places:

### GitLab repository

1. Fork the repo.
1. Edit the list of IP address inside the `score.sh` file.
1. On GitLab, go to `CI/CD` > `Schedules` and add a new schedule.
1. Provide all info to create a new pipeline schedule. Run the pipeline twice
   a day is good enough. Just remember: if you are using a private CI Runner,
   make sure that the server is online at the time you scheduled the pipeline.
1. Make sure that the project has the `Failed Pipeline` alert enabled.
   This way you will be notified by e-mail if a problem arise.
1. On GitLab, go to `Settings` > `Pages` and copy the page address on
  `Access pages` section. On gitlab.com, usually this name is
  `https://group_name.gitlab.io/project_name`.

### Prometheus server

Open your configuration file and add this section to your `scrape_configs`
section:

```yaml
scrape_configs:
  -
    job_name: sender_score
    scheme: https
    metrics_path: /project_name/metrics.txt
    scrape_interval: 60s
    scrape_timeout: 30s
    static_configs:
      -
        targets:
          - group_name.gitlab.io
```

### Grafana server

You can import the `dashboard.json` file to a new dashboard, to see the data
from Prometheus.

![Sender Score Dashboard](dashboard.png)

[senderscore]: https://senderscore.org
